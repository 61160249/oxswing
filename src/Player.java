

import java.io.Serializable;

public class Player implements Serializable{
    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }
    

    @Override
    public String toString() {
        return "Player{" + "name=" + name + '}';
    }

    public int getWin() {
        return this.win;
    }

    public int getLose() {
        return this.lose;
    }

    public int getDraw() {
        return this.draw;
    }
    
    public void win() {
        this.win++;
    }
    
    public void lose() {
        this.lose++;
    }
    
    public void draw() {
        this.draw++;
    }
    
}
